from seaborn import apionly as sns

import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib as mpl

plt.style.use(['seaborn-whitegrid', 'seaborn-ticks'])
import matplotlib.ticker as plticker
rcParams['figure.figsize'] = 8, 8

rcParams['axes.facecolor'] = 'FFFFFF'
rcParams['savefig.facecolor'] = 'FFFFFF'
rcParams['figure.facecolor'] = 'FFFFFF'

rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'

rcParams['mathtext.fontset'] = 'cm'
rcParams['mathtext.rm'] = 'serif'

rcParams.update({'figure.autolayout': True})

import os

import pandas as pd
import numpy as np

from tqdm import tqdm

#https://github.com/spotify/annoy
from annoy import AnnoyIndex

import uproot

from tqdm import tqdm

import subprocess as sp

smokeTest = False

tree = 'DSt_PiMTuple/DecayTree'

path = '/Users/MBP/Desktop/' if smokeTest else '/hdfs/user/ge19080/pid/'

fileNames = [
'00106050_00000001_1.pidcalib.root',
'00106050_00000300_1.pidcalib.root',
'00106050_00000001_1.pidcalib.root',

]

if not smokeTest : fileNames = os.listdir(path)

def fillNN(fileName, vs, nn):

    df = uproot.open(fileName)[tree].arrays(library = 'pd')

    # Try and remove as much background as possible
    df = df.query('Dst_M > 1995 and Dst_M < 2025')

    calibData = df[vs].to_numpy()

    # Add the data points to the NN trees
    for i in range(len(calibData)):
        nn.add_item(i, calibData[i])

def buildIndex():

    # Variables that the PID probability depends upon
    vs = ['probe_P', 'probe_PT', 'nSPDhits']

    nn = AnnoyIndex(3, 'euclidean')
    nn.on_disk_build('/tmp/pid_pi.ann')

    files = [f'{path}{f}' for f in fileNames]

    for f in tqdm(files): fillNN(f, vs, nn)

    nn.build(25)

    sp.call('/usr/bin/hadoop fs -put /tmp/pid_pi.ann pid_pi.ann', shell = True)

def test(calibData, pids, nn):

    plt.hist(pids, 100);
    plt.yscale('log')
    plt.savefig('pids.pdf')
    plt.clf()

    nTest = 4
    colours = ['blue', 'green', 'orange', 'red']
    testIndices = np.random.randint(0, len(calibData), size = 4)

    nnIdxs = [nn.get_nns_by_vector(calibData[i], 100, search_k = 100) for i in testIndices]

    bins = np.linspace(0, 1, 100)

    for i, idx in enumerate(nnIdxs):
        plt.hist(pids[idx], bins = bins, color = colours[i], label = str(pids[testIndices[i]]));

    plt.yscale('log')
    plt.legend(loc = 0)
    plt.savefig('nnPIDs.pdf')

if __name__ == '__main__':

    buildIndex()

    df = uproot.open(fileName)[tree].arrays(library = 'pd')
    df = df.query('Dst_M > 1995 and Dst_M < 2025')
    pids = df['probe_Brunel_MC15TuneV1_ProbNNpi'].to_numpy()
